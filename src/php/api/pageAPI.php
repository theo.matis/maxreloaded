<?php 
//Permet d'obtenir un module static (pre process sans changement)
function getStaticModule($moduleName){
    $pdo = new PDO('mysql:host=localhost;dbname=maxesportdb', 'maxesport', 'Oasis780');
    $statement = $pdo->prepare('SELECT `content` FROM `pagecomp` WHERE `name`= :name ');
    $statement->bindValue(':name', $moduleName, PDO::PARAM_STR);
    $statement->execute();
    $row = $statement->fetch(PDO::FETCH_ASSOC)["content"];
    if($row==""){
        echo "[ERROR] Unable to find module : ";
        echo $moduleName;
    }
    return $row;
}
//Permet d'ajouter un module static (pre process sans changement)
function addStaticModule($moduleName, $content)
{
    $pdo = new PDO('mysql:host=localhost;dbname=maxesportdb', 'maxesport', 'Oasis780');
    $statement = $pdo->prepare('SELECT `content` FROM `pagecomp` WHERE `name`= :name ');
    $statement->bindValue(':name', $moduleName, PDO::PARAM_STR);
    $statement->execute();
    $row = $statement->fetch(PDO::FETCH_ASSOC)["content"];
    if($row==""){
        $statement = $pdo->prepare('INSERT INTO `pagecomp`(`name`, `content`) VALUES (:name, :content)');
        $statement->bindValue(':name', $moduleName, PDO::PARAM_STR);
        $statement->bindValue(':content', $content, PDO::PARAM_STR);
        $statement->execute();
    } else {
        $statement = $pdo->prepare('UPDATE `pagecomp` SET `content`= :content WHERE `name`= :name');
        $statement->bindValue(':name', $moduleName, PDO::PARAM_STR);
        $statement->bindValue(':content', $content, PDO::PARAM_STR);
        $statement->execute();
    }
}
?>