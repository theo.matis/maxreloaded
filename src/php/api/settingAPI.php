<?php
//Get the associated setting.
function getSetting($setting)
{
    $pdo = new PDO('mysql:host=localhost;dbname=maxesportdb', 'maxesport', 'Oasis780');
    $statement = $pdo->prepare('SELECT `isEnabled` FROM `toggled` WHERE `name` =:setting');
    $statement->bindValue(':setting', $setting, PDO::PARAM_STR);
    $statement->execute();
    $row = $statement->fetch(PDO::FETCH_ASSOC)["isEnabled"];
    if ($row == "1") {
        return true;
    } elseif ($row == "0") {
        return false;
    } else {
        trigger_error("unable to find setting : '" . $setting . "'", E_USER_ERROR);
    }
}
