<!--MAXESPORT © index.html-->
<?php
require "src/php/api/settingAPI.php";
$maintenance = getSetting("maintenance");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <?php 
  require "src/php/api/pageAPI.php";
  echo getStaticModule("head");?>
  <noscript>
    <?php if (!isset($_GET['js'])) {
      echo '<meta http-equiv="refresh" content="0; url=?js=off" />';
    } ?>
  </noscript>
  <title>
    <?php
    if ($maintenance) {
      echo "MAINTENANCE";
    } else {
      echo "CHARGEMENT";
    }
    ?>
  </title>
</head>

<body>
  <?php if ($maintenance || !isset($_GET['js'])) {
      echo '
<img class="loading-logo bounce-in-top" src="src/img/logo_maxesport.svg" alt="loading">
<div class="loadbox">
  <div class="flicker slide">
  ';
      if (!$maintenance) {
        
        echo "CHARGEMENT"; 
      } else {
        echo "MAINTENANCE";
      }
      echo '
  </div>
  </div>
  ';
    } else {
      
      echo '<script>document.location.href="/"</script>';
      echo getStaticModule("header");
      addStaticModule("footer","test");
      echo getStaticModule("footer");
    } ?>
</body>


</html>